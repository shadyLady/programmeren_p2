﻿using UnityEngine;
using System.Collections;

public class Rule 
{
    char _predecessor;
    string _successor;

    public Rule(char a, string b)
    {
        _predecessor = a;
        _successor = b;
    }

    public char GetPredecessor()
    {
        return _predecessor;
    }
    public string GetSuccessor()
    {
        return _successor;
    }
}
