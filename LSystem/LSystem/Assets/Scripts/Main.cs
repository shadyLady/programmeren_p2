﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic; 

public class Main : MonoBehaviour
{
    private Rule[] _ruleset;
    private LSystem _lsystem;
    private Turtle _turtle;

    [SerializeField]
    private List<Branch> _tree = new List<Branch>(); 

    [SerializeField]
    private GameObject _branch;

    private int _generation = 0;
    private int _maxGeneration = 3;

    private bool _isGenerating = true; 

    [SerializeField] private Slider _generationSlider; 

    private void Awake()
    {
        _ruleset = new Rule[1];
        _ruleset[0] = new Rule('F', "FF+[+F-F-F]-[-F+F+F]");

        _lsystem = new LSystem("F", _ruleset);
        _turtle= new Turtle(_lsystem.GetSentence(), 0.5f, 30, _branch, new Vector3(0.0f, -0.5f, 0.0f));
    }

    private void Generate()
    {
        for(int i = 0; i < _maxGeneration; i++)
        {
            _lsystem.Generate();
            _turtle.UpdateSentence(_lsystem.GetSentence());
            AddToTree(_turtle.Build(), i);
        }

        _isGenerating = false;
    }
    private void Update()
    {
        if(_isGenerating)
        {
            Generate();
        }
    }
    private void AddToTree(List<Branch> b, int generation)
    {
        for(int i = 0; i < b.Count; i++)
        {
            _tree.Add(b[i]);
            b[i].Generation = generation;
            b[i].transform.parent = transform;
        }
    }
    public void UpdateGenerationSlider()
    {
        _generation = (int)_generationSlider.value; 

        for(int i = 0; i < _tree.Count; i++)
        {
            if(_tree[i].Generation >= _generation)
            {
                _tree[i].gameObject.SetActive(false);
            }
            else
            {
                _tree[i].gameObject.SetActive(true);
            }
        }
    }
}
