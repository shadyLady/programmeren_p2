﻿using UnityEngine;
using System.Collections;

public class Utilities : Singleton<Utilities>
{
    public Bounds Bounds()
    {
        Bounds bounds = new Bounds();

        bounds.Top = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 1.0f, Camera.main.transform.position.z)).y;
        bounds.Bottom = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 0.0f, Camera.main.transform.position.z)).y;
        bounds.Left = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 0.0f, Camera.main.transform.position.z)).x;
        bounds.Right = Camera.main.ViewportToWorldPoint(new Vector3(1.0f, 0.0f, Camera.main.transform.position.z)).x;

        return bounds;
    }
}
