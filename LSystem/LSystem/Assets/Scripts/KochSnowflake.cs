﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public class KochSnowflake : MonoBehaviour
{
    private Rule[] _ruleset;
    private LSystem _lsystem;
    private Turtle _turtle;

    private List<Branch> _snowFlake = new List<Branch>();

    private int _generation;

    private Vector3 _velocity;
    private float _gravity = -0.01f;
    private float _hor;
    private float _rotSpeed = 20.0f;
    private int _rotDirection = 1; 

    [SerializeField] private GameObject _line;

    private void Awake()
    {
        _ruleset = new Rule[1];
        _ruleset[0] = new Rule('F', "F+F-F-FF+F+F-F");
        _lsystem = new LSystem("F+F+F+F", _ruleset);

        float length = Random.Range(0.01f, 0.1f);

        _line.transform.localScale = new Vector3(length, length, length);
        _turtle = new Turtle(_lsystem.GetSentence(), length, 90, _line, transform.position);

        _lsystem.Generate();
        _turtle.UpdateSentence(_lsystem.GetSentence());
        AddToFlake(_turtle.Build());

        _turtle.Destroy();

        float chanceToLeft = 0.5f;

        if (Random.Range(0.0f, 1.0f) > chanceToLeft)
            _rotDirection = -1; 

        _hor = Random.Range(-0.01f, 0.01f);


    }
    private void Update()
    {
        _velocity.y += _gravity;
        _velocity.x = Mathf.Sin(Time.time * _hor);

        transform.position += _velocity * Time.deltaTime;
        transform.rotation *= Quaternion.Euler(0.0f, 0.0f, (_rotSpeed * _rotDirection) * Time.deltaTime);
        Border();
    }
    private void AddToFlake(List<Branch> s)
    {
        for (int i = 0; i < s.Count; i++)
        {
            _snowFlake.Add(s[i]);
            s[i].transform.parent = gameObject.transform;
        }
    }
    private void Border()
    {
        Vector3 p = transform.position + (_velocity * Time.deltaTime);
        Bounds bounds = Utilities.Instance.Bounds();

        if (p.y < bounds.Bottom + 3.5f)
        {
            p.y = bounds.Top;
            _velocity.y = -1.5f;
        }

        transform.position = p;
    }
}
