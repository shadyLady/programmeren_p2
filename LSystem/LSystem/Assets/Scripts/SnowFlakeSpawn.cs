﻿using UnityEngine;
using System.Collections;

public class SnowFlakeSpawn : MonoBehaviour
{
    [SerializeField] private int _amount;
    private int _currentAmount; 
    [SerializeField] private GameObject _snowFlake;

    [SerializeField] private float _spawnDelay;
    private float _timer; 

    private Bounds _bounds; 

    private void Start()
    {
        _bounds = Utilities.Instance.Bounds();
    }

    private void Update()
    {
        if(_currentAmount < _amount)
        {
            if (_timer < _spawnDelay)
                _timer += Time.deltaTime;
            else GenerateSnowFlakes();
        }
    }

    private void GenerateSnowFlakes()
    {
        Vector2 position = new Vector2(Random.Range(_bounds.Left, _bounds.Right), _bounds.Top + Random.Range(2.0f, 12.0f));

        GameObject flake = Instantiate(_snowFlake, position, Quaternion.identity) as GameObject;
        flake.transform.parent = transform;

        _currentAmount++;
        _timer = 0.0f; 
    }

}
