﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Flock
{
    private List<Boid> _boids; 

    public Flock()
    {
        _boids = new List<Boid>();
    }

    public void Provide()
    {
        for(int i = 0; i < _boids.Count; i++)
        {
            _boids[i].Flock(_boids);
        }
    }

    public void AddBoid(Boid boid)
    {
        _boids.Add(boid);
    }

    public List<Boid> GetBoids()
    {
        return _boids;
    }

}
