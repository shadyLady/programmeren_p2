﻿using UnityEngine;
using System.Collections;

public class Utilities : Singleton<Utilities>
{ 
    public Vector3 Subtract(Vector3 a, Vector3 b)
    {
        float x = a.x - b.x;
        float y = a.y - b.y;
        float z = a.z - b.z;

        return new Vector3(x, y, z);
    }

    public Vector3 ClampToValue(Vector3 a, float b)
    {
        if (a.x > 0)
        {
            if (a.x > b) a.x = b;
        }
        else
        {
            if (a.x < -b) a.x = -b;
        }


        if (a.y > 0)
        {
            if (a.y > b) a.y = b;
        }
        else
        {
            if (a.y < -b) a.y = -b;
        }


        if (a.z > 0)
        {
            if (a.z > b) a.z = b;
        }
        else
        {
            if (a.z < -b) a.z = -b;
        }



        return a;
    }

    public Bounds Bounds()
    {
        Bounds bounds = new Bounds();

        bounds.Top = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 1.0f, Camera.main.transform.position.z)).y;
        bounds.Bottom = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 0.0f, Camera.main.transform.position.z)).y;
        bounds.Left = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 0.0f,  Camera.main.transform.position.z)).x;
        bounds.Right = Camera.main.ViewportToWorldPoint(new Vector3(1.0f, 0.0f,  Camera.main.transform.position.z)).x;

        return bounds;
    }

}
