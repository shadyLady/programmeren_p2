﻿using UnityEngine;
using System.Collections;

public struct Bounds 
{
    public float Top;
    public float Bottom;
    public float Left;
    public float Right;
}
