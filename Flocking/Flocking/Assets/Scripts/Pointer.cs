﻿using UnityEngine;
using System.Collections;

public class Pointer : MonoBehaviour
{
    private bool _isBait = true;
    
    private void Awake()
    {
        GetComponent<Renderer>().material.color = Color.green;
    } 

    private void Update()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.z = - 4;
        transform.position = mousePosition; 
    }
}
