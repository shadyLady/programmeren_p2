﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Boid : MonoBehaviour
{
    [SerializeField]
    private Vector3 _velocity;
    public Vector3 Velocity { get { return _velocity; } }

    private Vector3 _acceleration;

    private float _maxForce;
    private float _maxSpeed;
    private float _radius;

    private float _cohesionWeight;
    private float _seperationWeight;
    private float _alignmentWeight;

    public void AssignValues(float x, float y)
    {
        _acceleration = Vector3.zero;
        _velocity = new Vector3(Random.Range(-1.0f, 1.0f), -1.0f, 0.0f);
        transform.position = new Vector3(x, y);

        _radius = GetComponent<SpriteRenderer>().bounds.size.x * 0.5f;
        _maxForce = 0.5f;
        _maxSpeed = 2.0f;
    }

    public void UpdateWeightValues(float alignment, float seperation, float cohesion)
    {
        _cohesionWeight = cohesion;
        _seperationWeight = seperation;
        _alignmentWeight = alignment;
    }

    public void Flock(List<Boid> boids)
    {
        Vector3 seperation = Seperation(boids);
        Vector3 alignment = Alignment(boids);
        Vector3 cohesion = Cohesion(boids);

        cohesion *= _cohesionWeight;
        alignment *= _alignmentWeight;
        seperation *= _seperationWeight;

        ApplyForce(seperation);
        ApplyForce(alignment);
        ApplyForce(cohesion);

        Apply();
        Border();
    }

    private void Border()
    {
        Vector3 p = transform.position + _velocity * Time.deltaTime;
        Bounds b = Utilities.Instance.Bounds();
        if (p.x + _radius > b.Right)
        {
            float dist = b.Right - (p.x + _radius);
            transform.position += new Vector3(dist, 0.0f, 0.0f);
            _velocity.x *=  -1;

        }
        if (p.x - _radius < b.Left)
        {
            float dist = (p.x - _radius) - b.Left;
            transform.position -= new Vector3(dist, 0.0f, 0.0f);
            _velocity.x *= -1;
        }

        if (p.y + _radius > b.Top)
        {

            float dist = b.Top - (p.y + _radius);
            transform.position += new Vector3(0.0f, dist, 0.0f);
            _velocity.y *= -1;
        }
        if (p.y - _radius < b.Bottom)
        {
            float dist = (p.y - _radius) - b.Bottom;
            transform.position -= new Vector3(0.0f, dist, 0.0f);
            _velocity.y *= -1;
        }
    }
    private void Apply()
    {
        _velocity += _acceleration;
        _velocity.Normalize();

        float angle = Mathf.Atan2(-_velocity.x, _velocity.y) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        _velocity.z = 0;
        _velocity *= _maxSpeed;
        transform.position += _velocity * Time.deltaTime;

        //Reset acceleration
        _acceleration = Vector3.zero;
    }

    private void ApplyForce(Vector3 force)
    {
        _acceleration += force; 
    }

    private Vector3 Cohesion(List<Boid> boids)
    {
        //Find center location of all boids and steer towards said location
        float range = 5.0f;
        Vector3 total = Vector3.zero;

        int count = 0; 

        for(int i = 0; i < boids.Count; i++)
        {
            float distance = Vector3.Distance(boids[i].transform.position, transform.position);

            if(boids[i] != this)
            {
                if (distance < range)
                {
                    total += boids[i].transform.position;
                    count++;
                }
            }
        }

        if(count > 0)
        {
            total /= count;

            Vector3 direction = total - transform.position;
            direction.Normalize();

            return direction;
        }
        return Vector3.zero;
    }

    private Vector3 Alignment(List<Boid> boids)
    {
        //Get the average velocity of each boid within reach 
        float range = 5.0f;
        Vector3 total = Vector3.zero;

        //Keep track of amount of boids handled for later calculations 
        int count = 0;  

        for(int i = 0; i < boids.Count; i++)
        {
            float distance = Vector3.Distance(boids[i].transform.position, transform.position);

            if(boids[i] != this)
            {
                if (distance < range)
                {
                    total += boids[i].Velocity;
                    count++;
                }
            }
        }

        if(count > 0)
        {
            total /= (float)count;
            total.Normalize();

            return total;
        }

        return Vector3.zero;
    }

    private Vector3 Seperation(List<Boid> boids)
    {
        float desiredSeperation = 8.0f;
        Vector3 direction = Vector3.zero;

        //Keep track of the amount of boids steered towards
        int count = 0; 

        for(int i = 0; i < boids.Count; i++)
        {
            float distance = Vector3.Distance(boids[i].transform.position, transform.position);

            if(distance > 0 && (distance < desiredSeperation))
            {
                Vector3 difference = transform.position - boids[i].transform.position;
                difference.Normalize();
                difference /= distance;
                direction += difference;
                count++; 
            }
        }

        //Divide by amount of boids steered towards to get average
        if (count > 0)
            direction /= (float)count;

        //If our new vector is 0 we can leave out changes
        if(direction != Vector3.zero)
        {
            direction.Normalize();
            direction *= _maxSpeed;

            direction = Utilities.Instance.Subtract(direction, _velocity);
            direction = Utilities.Instance.ClampToValue(direction, _maxForce);
        }

        return direction;

    }
}
