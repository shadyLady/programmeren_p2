﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Main : MonoBehaviour
{
    Flock _flock;

    [SerializeField] [Range(0, 1)] private float _alignmentWeight;
    [SerializeField] [Range(0, 1)] private float _seperationWeight;
    [SerializeField] [Range(0, 1)] private float _cohesionWeight;

    [SerializeField] private Slider _alignmentSlider;
    [SerializeField] private Slider _cohesionSlider;
    [SerializeField] private Slider _seperationSlider;

    private List<Flock> _flockList;

    private int _minFlockSize = 10;
    private int _maxFlockSize = 30;

    private int _numberOfFlocks = 1;

    private void Start()
    {
        //Cursor.visible = false;

        Bounds bounds = Utilities.Instance.Bounds();

        _flockList = new List<Flock>();

        for(int f = 0; f < _numberOfFlocks; f++)
        {
            Flock flock = new Flock();
            Vector2 startPos = new Vector2(Random.Range(bounds.Left + 2, bounds.Right - 2), Random.Range(bounds.Top - 2, bounds.Bottom + 2));

            int size = Random.Range(_minFlockSize, _maxFlockSize);

            for (int i = 0; i < size; i++)
            {
                Boid boid = Instantiate(Resources.Load<Boid>("Prefabs/Tadpole"));
                boid.AssignValues(startPos.x, startPos.y);
                boid.UpdateWeightValues(_alignmentWeight, _seperationWeight, _cohesionWeight);
                flock.AddBoid(boid);
            }

            _flockList.Add(flock);
        }
    }
    private void Update()
    {
        if (_flockList.Count <= 0)
            return;

        for(int i = 0; i < _flockList.Count; i++)
            _flockList[i].Provide();
    }
    public void UpdateValues()
    {

        _alignmentWeight = _alignmentSlider.value;
        _cohesionWeight = _cohesionSlider.value;
        _seperationWeight = _seperationSlider.value;

        for(int i = 0; i < _flockList.Count; i++)
        {
            List<Boid> boids = _flockList[i].GetBoids();

            for (int j = 0; j < boids.Count; j++)
            {
                boids[j].UpdateWeightValues(_alignmentWeight, _seperationWeight, _cohesionWeight);
            }
        }
    }
}
