﻿using UnityEngine;
using System.Collections;

public class GridManager : MonoBehaviour 
{
    [SerializeField]
    private GameManager m_gameManager;

    private Cell[,] m_cells;

    private bool[,] m_layout; 
    public bool[,] m_Layout
    {
        get { return m_layout; }
    }

    [SerializeField]
    private Cell m_tilePrefab;

    [SerializeField]
    private int m_gridWidth;
    public int m_GridWidth
    {
        get { return m_gridWidth; }
    }

    [SerializeField]
    private int m_gridHeight;
    public int m_GridHeight
    {
        get { return m_gridHeight; }
    }

    [SerializeField]
    private float m_tileWidth;

    [SerializeField]
    private float m_tileHeight; 

    private void Awake()
    {
        m_tileHeight = m_tilePrefab.GetComponent<SpriteRenderer>().bounds.size.y;
        m_tileWidth = m_tilePrefab.GetComponent<SpriteRenderer>().bounds.size.x;

        CreateGrid();
    }
    private void Update()
    {
        if (m_gameManager.m_GameState != GameState.STATE_GAME)
            return;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit; 

        if(Input.GetMouseButtonDown(0))
        {
            if(Physics.Raycast(ray, out hit))
            {
                if(hit.collider != null)
                {
                    Debug.Log("Collider hit" + hit.collider.name);
                    Cell hitCell = hit.collider.GetComponent<Cell>();

                    if(hitCell != null)
                    {
                        ToggleCellState(hitCell.m_X, hitCell.m_Y);

                        Debug.Log(hitCell.name);
                    }
                }
            }
        }
    }
    private void CreateGrid()
    {
        m_cells = new Cell[m_gridHeight, m_gridWidth];
        m_layout = new bool[m_gridHeight, m_gridWidth];

        for(int y = 0; y < m_gridHeight; y++)
        {
            for(int x = 0; x < m_gridWidth; x++)
            {
                Vector3 tilePosition = new Vector3(x * m_tileWidth, y * m_tileHeight);

                m_cells[y,x] = Instantiate(m_tilePrefab, tilePosition, Quaternion.identity) as Cell;
                m_cells[y, x].gameObject.name = "Tile_" + y + "_" + x;
                m_cells[y, x].gameObject.transform.parent = transform;

                m_cells[y, x].m_X = x;
                m_cells[y, x].m_Y = y;

                m_layout[y,x] = false;
            }
        }
    }
    private void ToggleCellState(int x, int y)
    {
        m_layout[y, x] = !m_layout[y, x];
        m_cells[y, x].ToggleActivity(m_layout[y, x]);
    }
    public void UpdateGrid(bool[,] newLayout)
    {
        m_layout = newLayout;

        for(int y = 0; y < newLayout.GetLength(0); y++)
        {
            for(int x = 0; x < newLayout.GetLength(1); x++)
            {
                m_cells[y, x].ToggleActivity(m_layout[y, x]);
            }
        }
    }

    public Cell GetCellAtPosition(int x, int y)
    {
        return m_cells[y, x];
    }
}
