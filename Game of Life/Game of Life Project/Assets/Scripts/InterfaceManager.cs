﻿using UnityEngine;
using System.Collections;

public class InterfaceManager : MonoBehaviour
{
    [SerializeField]
    private GameObject m_introductionInterface;

    [SerializeField]
    private GameObject m_gameInterface;

    [SerializeField]
    private GameObject m_simulationInterface;

    [SerializeField]
    private GameManager m_gameManager;

    [SerializeField]
    private GameInterface m_gameInterfaceScript;
    public GameInterface GameInterface
    {
        get { return m_gameInterfaceScript; }
    }

    private void Start()
    {
        m_gameManager.OnGameStart += HideIntroduction;
        m_gameManager.OnGameStart += HideSimulation;
        m_gameManager.OnGameStart += ShowGame;

        m_gameManager.OnSimulationStart += HideGame;
        m_gameManager.OnSimulationStart += ShowSimulation;
    }

    private void ShowIntroduction()
    {
        m_introductionInterface.SetActive(true);
    }

    private void HideIntroduction()
    {
        m_introductionInterface.SetActive(false);
    }

    private void ShowGame()
    {
        m_gameInterface.SetActive(true);
    }

    private void HideGame()
    {
        m_gameInterface.SetActive(false);
    }

    private void ShowSimulation()
    {
        m_simulationInterface.SetActive(true);
    }

    private void HideSimulation()
    {
        m_simulationInterface.SetActive(false);
    }
}
