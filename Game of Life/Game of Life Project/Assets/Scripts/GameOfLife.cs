﻿using UnityEngine;
using System.Collections;

public class GameOfLife : MonoBehaviour 
{
    [SerializeField]
    private GridManager m_gridManager;

    private IEnumerator _generationRunning; 
    
    private void Generate()
    {
        bool[,] currentLayout = m_gridManager.m_Layout;
        bool[,] tmp = GenerateNextState(currentLayout);
        m_gridManager.UpdateGrid(tmp);
    }
    private bool[,] GenerateNextState(bool[,] layout)
    {
        bool[,] newLayout = layout.Clone() as bool[,];

        for(int y = 0; y < m_gridManager.m_GridHeight; y++)
        {
            for(int x = 0; x < m_gridManager.m_GridWidth; x++)
            {

                int neighbourCount = NeighbourCount(layout, x, y);

                bool cellState = layout[y, x];

                if(cellState == true)
                {
                    if(neighbourCount < 2)
                    {
                        cellState = false;
                    }
                    else if(neighbourCount > 3)
                    {
                        cellState = false;
                    }
                }
                else
                {
                    if (neighbourCount == 3)
                    {
                        cellState = true;
                    }
                }


                newLayout[y, x] = cellState;
            }
        }

        return newLayout;
    }
    private int NeighbourCount(bool[,] layout, int x, int y)
    {
        int neighbourCount = 0; 

        for(int i = -1; i < 2; i++)
        {
            for (int j = -1; j < 2; j++)
            {

                if (IsValidCellCoord(x + j, y + i))
                {
                    if (i == 0 && j == 0)
                        continue;

                    if (layout[y + i, x + j] == true)
                    {
                        neighbourCount++;
                    }
                }
            }
        }

        m_gridManager.GetCellAtPosition(x, y).m_NeighbourCount = neighbourCount;
        return neighbourCount;
    }
    private bool IsValidCellCoord(int x, int y)
    {
        if(y >= 0 && y < m_gridManager.m_GridHeight)
        {
            if(x >= 0 && x < m_gridManager.m_GridWidth)
            {
                return true; 
            }
        }
        return false;

    }
    public void StartGeneration(int amount, float duration)
    {
        if (_generationRunning != null)
            return;

        _generationRunning = RunGeneration(amount, duration);

        StartCoroutine(_generationRunning);
    }
    public IEnumerator RunGeneration(int amount, float duration)
    {
        int generation = 0;

        while (generation < amount)
        {
            Generate();
            generation++;

            Debug.Log(generation);

            yield return new WaitForSeconds(duration);
        }

        _generationRunning = null;
    }

    public void Reset()
    {
        if(_generationRunning != null)
        {
            StopCoroutine(_generationRunning);
            _generationRunning = null;
        }

    }
}
