﻿using UnityEngine;
using System.Collections;

public class Cell : MonoBehaviour 
{
    private int m_x;
    public int m_X
    {
        get { return m_x; }
        set { m_x = value; }
    }

    private int m_y;
    public int m_Y
    {
        get { return m_y; }
        set { m_y = value; }
    }

    [SerializeField]
    private int m_neighbourCount; 
    public int m_NeighbourCount
    {
        get { return m_neighbourCount; }
        set { m_neighbourCount = value; }
    }

    [SerializeField]
    private bool m_isActive; 

    public void ToggleActivity(bool active)
    {
        m_isActive = active;

        if(active)
            gameObject.GetComponent<SpriteRenderer>().color = Color.black;
        else 
            gameObject.GetComponent<SpriteRenderer>().color = Color.white; 
    }
}
