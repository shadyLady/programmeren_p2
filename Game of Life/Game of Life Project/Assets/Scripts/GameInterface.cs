﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameInterface : MonoBehaviour
{
    [SerializeField] private Text _generationText;
    [SerializeField] private Text _speedText;

    [SerializeField] private Slider _generationSlider;
    public Slider GenerationSlider { get { return _generationSlider; } }

    [SerializeField] private Slider _speedSlider; 
    public Slider SpeedSlider { get { return _speedSlider; } }
    public void UpdateText()
    {
        _generationText.text = "Generation amount : " + _generationSlider.value;
        _speedText.text = "Generation speed : " +  System.Math.Round(_speedSlider.value, 1);
    }
}
