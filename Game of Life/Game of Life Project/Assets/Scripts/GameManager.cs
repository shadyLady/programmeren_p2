﻿using UnityEngine;
using System.Collections;

public delegate void VoidDelegate();

public enum GameState
{
    STATE_INTRODUCTION,
    STATE_GAME,
    STATE_SIMULATION
};

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameState m_gameState;
    public GameState m_GameState
    {
        get { return m_gameState; }
    }

    [SerializeField]
    private GameOfLife m_gameOfLife;

    [SerializeField]
    private InterfaceManager m_interfaceManager;

    private event VoidDelegate m_onGameStart;
    public VoidDelegate OnGameStart
    {
        get { return m_onGameStart; }
        set { m_onGameStart = value; }
    }

    private event VoidDelegate m_onSimulationStart;
    public VoidDelegate OnSimulationStart
    {
        get { return m_onSimulationStart; }
        set { m_onSimulationStart = value; }
    }

    private void Awake()
    {
        m_gameState = GameState.STATE_INTRODUCTION;
    }
    public void StartGame()
    {
        if(m_gameState == GameState.STATE_GAME)
            m_gameOfLife.Reset();

        if (m_onGameStart != null)
            m_onGameStart();

        m_gameState = GameState.STATE_GAME;
    }
    public void RunSimulation()
    {
        if (m_gameState != GameState.STATE_GAME)
            return;

        if (m_onSimulationStart != null)
            m_onSimulationStart();

        //Should be defined by user
        int amount = (int)m_interfaceManager.GameInterface.GenerationSlider.value;
        float duration = m_interfaceManager.GameInterface.SpeedSlider.value;
        m_gameOfLife.StartGeneration(amount, duration);
    }
}
