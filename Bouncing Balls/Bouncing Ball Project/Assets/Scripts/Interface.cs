﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Interface : MonoBehaviour
{
    private Main _main;
    [SerializeField] private Slider _bounciness;
    [SerializeField] private Slider _radius; 

    private void Start()
    {
        _main = FindObjectOfType<Main>();
    }

    public void CreateBall()
    {
        _main.AddBall(_radius.value, _bounciness.value);
    }
}
