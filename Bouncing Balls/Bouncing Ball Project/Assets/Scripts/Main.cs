﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Main : MonoBehaviour
{
    private List<Ball> _balls;
    [SerializeField] private Material _lineMaterial;

    private Ball _selected;

    private Vector3 _start;
    private Vector3 _end;

    [SerializeField] private float _multiplier;

    private void Start()
    {
        Bounds bounds = Utilities.Instance.Bounds();

        _balls = new List<Ball>();

        for(int i = 0; i < 1; i++)
        {
            Ball b = Instantiate(Resources.Load<Ball>("Prefabs/Ball"));
            b.name = "Ball " + i;
            Vector3 pos = new Vector3(Random.Range(bounds.Left + 2, bounds.Right - 2), Random.Range(bounds.Top - 2, bounds.Bottom + 2), 0.0f);
            float radius = Random.Range(0.1f, 0.6f);

            float mass = radius * 1.2f;

            b.Assign(pos, radius, mass, 0.8f);

            _balls.Add(b);    
        }        
    }
    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            for(int i = 0; i < _balls.Count; i++)
            {
                Transform bPos = _balls[i].transform;
                float bRad = _balls[i].Radius;

                if(mousePosition.x < bPos.position.x + bRad && mousePosition.x > bPos.position.x - bRad && mousePosition.y < bPos.position.y + bRad && mousePosition.y > bPos.position.y - bRad)
                {
                    _selected = _balls[i];
                    _start = mousePosition;
                    _start.z = 0.0f;
                } 
            }
        }

        if(_selected != null)
        {
            Vector3 targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            targetPos.z = 0.0f;
            _selected.transform.position = targetPos;

            if(Input.GetMouseButtonUp(0))
            {
                _end = targetPos;
                _end.z = 0.0f;



                float distance = Vector3.Distance(_start, _end);

                Vector3 direction = _end - _start;
                direction.Normalize();

                _selected.ApplyForce(direction * (distance * _multiplier));
                _selected = null;
            }
        }
    }
    public void AddBall(float radius, float bounciness)
    {
        Bounds bounds = Utilities.Instance.Bounds();
        Ball ball = Instantiate(Resources.Load<Ball>("Prefabs/Ball"));

        Vector3 pos = new Vector3(Random.Range(bounds.Left + 2, bounds.Right - 2), Random.Range(bounds.Top - 2, bounds.Bottom + 2), 0.0f);
        float mass = radius * 1.2f;

        Spring spring = gameObject.AddComponent<Spring>() ;
        spring.Assign(ball, _balls[_balls.Count - 1], _lineMaterial);
        ball.Assign(pos, radius, mass, bounciness);
        ball.AttachSpring(spring);
        
        _balls.Add(ball);
        ball.name = "Ball " + _balls.Count;
    }
    /*
    private void OnRenderObject()
    {
        if(_lineMaterial == null)
        {
            Debug.Log("Please assign a material");
            return;
        }

        _lineMaterial.SetPass(0);

        GL.PushMatrix();
        GL.MultMatrix(transform.localToWorldMatrix);

        GL.Begin(GL.LINES);
        GL.Color(Color.black);

        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.z = 0;

        for (int i = 0; i < _balls.Count; i++)
        {
            GL.Vertex3(mousePosition.x, mousePosition.y, mousePosition.z);

            float length = Vector3.Distance(_balls[i].transform.position, mousePosition);
            length -= _balls[i].Radius;

            Vector3 direction = _balls[i].transform.position - mousePosition;
            direction.Normalize();

            GL.Vertex3(mousePosition.x + ( direction.x * length), mousePosition.y + ( direction.y * length), 0.0f);
        }

        GL.End();
        GL.PopMatrix();
    }*/
}
