﻿using UnityEngine;
using System.Collections;

public class Spring : MonoBehaviour
{
    private float _springConstant = 0.8f;
    private Vector3 _size;
    private float _length = 2.0f;
    private Ball _pulledBall;
    private Ball _owner;
    private Material _lineMaterial;
    
    public void Assign(Ball owner, Ball ball, Material lineMaterial)
    {
        _lineMaterial = lineMaterial;
        _pulledBall = ball;
        _owner = owner;
        //Should be direction between the first and second ball?
        _size = _owner.transform.position - ball.transform.position;
        _size.Normalize();
        _size *= _length;
    }

    //C = F * u, dus u = F * C
    public void Pull(Vector3 force)
    {
        Vector3 displacement = force * _springConstant;
        //TODO : Now get the pulled ball in to the right position
    }

    private void OnRenderObject()
    {
        if (_lineMaterial == null)
        {
            Debug.Log("Please assign a material");
            return;
        }

        _lineMaterial.SetPass(0);

        GL.PushMatrix();
        GL.MultMatrix(transform.localToWorldMatrix);

        GL.Begin(GL.LINES);
        GL.Color(Color.black);

        Vector3 direction = _pulledBall.transform.position - _owner.transform.position;
        direction.Normalize();

        GL.Vertex3(_owner.transform.position.x + (_owner.Radius * direction.x), _owner.transform.position.y + (_owner.Radius * direction.y), 0.0f);
        GL.Vertex3(_pulledBall.transform.position.x + (_pulledBall.Radius * -direction.x), _pulledBall.transform.position.y + (_pulledBall.Radius * -direction.y), 0.0f);

        GL.End();
        GL.PopMatrix();
    }
}
