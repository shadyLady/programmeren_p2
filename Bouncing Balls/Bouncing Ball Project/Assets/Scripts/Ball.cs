﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{
    private Vector3 _velocity;
    public Vector3 Velocity
    {
        get { return _velocity; }
        set { _velocity = value; }
    }
    private Vector3 _acceleration; 

    private float _maxSpeed = 5.0f; 
    private float _radius = 0.2f;
    public float Radius { get { return _radius; } }
    [SerializeField] private Material _lineMaterial;
    private int lineCount = 360;
    private float _mass;
    private float _restitution = 0.8f; 
    private float _airFriction = 0.2f;

    private Spring _spring;

    private void Awake()
    {
        _acceleration = new Vector3(-0.000f, -0.03f);
        _velocity = Vector3.zero;
    }
    public void Assign(Vector3 position, float radius, float mass, float restitution)
    {
        _mass = mass;
        _radius = radius;
        _restitution = restitution;
        transform.position = position;
    }
    public void AttachSpring(Spring spring)
    {
        _spring = spring;
    }
    private void Update()
    {
        _velocity.y -= 0.08f;
        _velocity += _acceleration * _mass;
        _velocity = Utilities.Instance.ClampVectorToFloat(_velocity, _maxSpeed);
        Border();
        transform.Translate(_velocity * Time.deltaTime);

        if(_spring != null)
            _spring.Pull(_velocity * Time.deltaTime);

        _acceleration = Vector3.zero;
    }
    public void ApplyForce(Vector3 force)
    {
        _acceleration += force;
    }
    private void Border()
    {
        Vector3 p = transform.position + (_velocity * Time.deltaTime);
        Bounds b = Utilities.Instance.Bounds();

        if(p.x + _radius > b.Right)
        {
            float dist = b.Right - (p.x + _radius);
            transform.position += new Vector3(dist, 0.0f, 0.0f);
            _velocity.x = (_velocity.x * _restitution) * -1;

        }
        if (p.x - _radius < b.Left)
        {
            float dist = (p.x - _radius) - b.Left;
            transform.position -= new Vector3(dist, 0.0f, 0.0f);
            _velocity.x = (_velocity.x * _restitution) * -1;
        }

        if(p.y + _radius > b.Top)
        {

            float dist = b.Top - (p.y + _radius);
            transform.position += new Vector3(0.0f, dist, 0.0f);
            _velocity.y = (_velocity.y * _restitution) * -1;
        }
        if(p.y - _radius < b.Bottom)
        {
            float dist = (p.y - _radius) - b.Bottom;
            transform.position -= new Vector3(0.0f, dist, 0.0f);
            _velocity.y = (_velocity.y * _restitution) * -1;
        }
    }
    private void OnRenderObject()
    {
        if(_lineMaterial == null)
        {
            Debug.Log("Please assign a material");
            return;
        }

        _lineMaterial.SetPass(0);
        GL.PushMatrix();
        GL.MultMatrix(transform.localToWorldMatrix);

        GL.Begin(GL.LINES);
        
        for(int i = 0; i < lineCount; i++)
        {
            float angle = i / (float)lineCount;
            float newAngle = angle * Mathf.PI * 2;
            GL.Color(Color.red);

            GL.Vertex3(Mathf.Cos(newAngle) * _radius, Mathf.Sin(newAngle) * _radius, 0);
        }

        GL.End();
        GL.PopMatrix();
    }
}
